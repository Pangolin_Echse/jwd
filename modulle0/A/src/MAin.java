import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

//import static com.sun.corba.se.impl.io.ObjectStreamClassCorbaExt.getDeclaredMethods;

public class MAin {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Task task=new Task();
        Class<? extends Task> TaskClass=task.getClass();
        for (int i=1;i<=5;i++){
            System.out.println("Task "+i+"\n---------------------------");
            Method m=TaskClass.getMethod("task"+i);
            m.invoke(task);
            System.out.println("---------------------------");
        }
    }

}
class Task{
    public void task1(){

        //  System.out.println("\nTask1--------------------");
        int sum=0;
        int k=(int)(Math.random()*10+1);
        System.out.println("K: "+k);

        int arrSize=(int)(Math.random()*10+5);
        int arr[]=new int[arrSize];
        for (int i=0;i<arr.length;i++){
            arr[i]=(int)(Math.random()*100);
            System.out.print(arr[i]+" ");
        }

        for(int i=0;i<arr.length;i++){
            if(arr[i]%k==0){
                sum+=arr[i];
            }

        }
        System.out.println("\nSum:"+sum);
    }

    public void task2(){
        int count=0;
        int arrSize=10;
        int arr[]={1,2,3,0,65,0,343,0,0,10};
        for (int i=0;i<arr.length;i++){
            //    arr[i]=(int)(Math.random()*2-1);
            System.out.print(arr[i]+" ");
        }

        for(int i=0;i<arr.length;i++){
            if(arr[i]==0){
                count++;
            }
        }
        //arrSize=count;
        int arr2[]=new int[count];
        int j=0;
        for(int i=0;i<arrSize;i++){
            if(arr[i]==0){
                arr2[j]=i;
                j++;
            }
        }
        System.out.println();
        for (int i=0;i<count;i++){
            //  arr[i]=(Math.random());
            System.out.print(arr2[i]+" ");
        }
        System.out.println();
    }

    public void task3(){

        int arrSize=(int)(Math.random()*10+5);
        int arr[]=new int[arrSize];
        for (int i=0;i<arr.length;i++){
            arr[i]=(int)(Math.random()*100-50);
            System.out.print(arr[i]+" ");
        }

        if(arr[0]<0){
            System.out.println("\nNegative");
        }
        else {
            System.out.println("\nPositive");
        }

    }

    public void task4(){
        int arrSize=(int)(Math.random()*10+5);
        int arr[]=new int[arrSize];
        for (int i=0;i<arr.length;i++){
            arr[i]=(int)(Math.random()*100-50);
            System.out.print(arr[i]+" ");
        }
        int count=0;
        for (int i=1;i<arr.length;i++){
            if(arr[i]>arr[i-1])
                count++;
        }
        System.out.println();

        if(count==arr.length-1)
            System.out.println("YES");
        else System.out.println("NO");
    }
    public void task5(){
        int arrSize=(int)(Math.random()*10+5);
        int arr[]=new int[arrSize];
        for (int i=0;i<arr.length;i++){
            arr[i]=(int)(Math.random()*100-50);
            System.out.print(arr[i]+" ");
        }
        System.out.println();
        int count=0;
        for (int i=0;i<arr.length;i++){

            if(i%2==0){
                count=1;
                System.out.print(arr[i]+" ");}
        }
        if(count==0)
            System.out.print("too short ");
        System.out.println();
    }
}
