import java.io.Serializable;
import java.math.BigInteger;
import java.util.*;

public class Counts {
 TreeMap<Integer ,Count > counts = new TreeMap<Integer, Count>();
 void blok(int num){
     counts.get(num).status="Block";
 }
int sum(){
     List<Count> c=new ArrayList<>(counts.values());
int sum=c.get(0).sum;
    for(int i =1;i<c.size();i++) {
        sum+=c.get(i).sum;
     }
    return sum;
 }
 int sumPos(){
     List<Count> c=new ArrayList<>(counts.values());
     int sum=0;
     for(int i =1;i<c.size();i++) {
         if(c.get(i).sum>0)
         sum+=c.get(i).sum;
     }
     return sum;
 }
    int sumNeg(){
        List<Count> c=new ArrayList<>(counts.values());
        int sum=0;
        for(int i =1;i<c.size();i++) {
            if(c.get(i).sum<0)
                sum+=c.get(i).sum;
        }
        return sum;
    }
    Count find(int n){
     return counts.get(n);
    }
    List<Count> sort(){
        List<Count> c=new ArrayList<>(counts.values());
        Collections.sort(c,Count.COMPARE_BY_SUM);
        return c;
    }}
class Count implements Serializable {
    int num;
    int sum;
    String status ="Unblock";
    public static java.util.Comparator<Count> COMPARE_BY_SUM = new java.util.Comparator<Count>() {
        public int compare(Count one, Count other) {
            return one.sum-other.sum;
        }

    };}





