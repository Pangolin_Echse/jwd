package Service;

import Model.Auto;
import Model.Engine;
import Model.Wheel;

import java.util.List;

public interface AutoService {

    Engine getEngine();
    void setNumber(Auto auto, String number);
    void startEngine(Auto auto);
    void addFuel(Auto auto);
    void stopEngine(Auto auto);
    void setNewWheelsList(Auto auto);
    List<Wheel> getNewWheelsList();
    void go(Auto auto);
}
