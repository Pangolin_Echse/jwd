package Service;

import Model.Auto;
import Model.Engine;
import Model.GasEngine;
import Model.Wheel;

import java.util.List;

public class SimpleAutoService implements AutoService{

    public Engine getEngine() {
        return new GasEngine("ON");
    }

    @Override
    public void setNumber(Auto auto, String number) {
        auto.setNumber(number);
    }

    @Override
    public void startEngine(Auto auto) {
        if (auto.fuelTank.getFuel() != 0) {
            auto.getEngine().start();
            System.out.println("Двигатель заведен");
        } else {
            System.out.println("Нет топлива");
        }
    }

    @Override
    public void stopEngine(Auto auto) {
        auto.getEngine().stop();
        System.out.println("Двигатель заглушен");
    }

    @Override
    public void addFuel(Car car) {
        car.setFuel(car.getFuel() + 20);
    }


    @Override
    public List<Wheel> getNewWheelsList() {

        List<Wheel> wheelList = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            wheelList.add(new Wheel("Колесо" + (int) (Math.random() * 10)));
        }
        return wheelList;
    }

    @Override
    public void setNewWheelsList(Car car) {

        List<Wheel> wheelList = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            wheelList.add(new Wheel("Колесо" + (int) (Math.random() * 10)));
        }
        car.setWheelList(wheelList);
    }

    @Override
    public void go(Car car) {
        if (!(car.getNumber().equalsIgnoreCase("")) && car.getEngine().isWorking()) {
            System.out.println("Поехали");
        } else if (!car.getEngine().isWorking()) {
            System.out.println("Заведите двигатель");
        }

    }
