package Model;

public interface Engine {
    String run();
    boolean start();
    void stop();
}
