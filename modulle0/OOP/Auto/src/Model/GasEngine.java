package Model;

public class GasEngine implements Engine {
    String status;


    @Override
    public String run() {
        String energy="Energy";
        return energy;
    }

    @Override
    public boolean start() {
        status="ON";
        return true;
    }

    @Override
    public void stop() {
        status="off";
    }

    public GasEngine(String s){
        status=s;
    }

}
