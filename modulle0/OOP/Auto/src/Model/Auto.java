package Model;

import com.sun.tools.javac.util.List;


public class Auto {
    private Engine engine;
    private String number;
    public  FuelTank fuelTank;
   private List<Wheel> wheels;


    public Auto(Engine engine, List<Wheel> wheels, String number, FuelTank fuelTank) {
        this.engine = engine;
        this.number = number;
        this.fuelTank=fuelTank;
        this.wheels = wheels;
    }


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public void setWheelList(List<Wheel> wheels) {
        this.wheels = wheels;
    }

    

}
