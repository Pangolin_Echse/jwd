import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

//import static com.sun.corba.se.impl.io.ObjectStreamClassCorbaExt.getDeclaredMethods;

public class Decomposition {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        Task task=new Task();
        Class<? extends Task> TaskClass=task.getClass();
        for (int i=16;i<=16;i++){
            System.out.println("Task "+i+"\n---------------------------");
            Method m=TaskClass.getMethod("task"+i);
            m.invoke(task);
            System.out.println("---------------------------");
        }
    }

}
class Task{
    public void task1() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        double x1=in1.nextDouble();
        double x2=in1.nextDouble();
        double x3=in1.nextDouble();
        double y1=in1.nextDouble();
        double y2=in1.nextDouble();
        double y3=in1.nextDouble();
        System.out.println(space(x1,x2,x3,y1,y2,y3));

    }
    double lenght (double x1,double x2,double y1,double y2){
        return Math.sqrt(Math.pow(Math.abs(x1-x2),2)+Math.pow(Math.abs(y1-y2),2));
    }
    public double space(double x1,double x2,double x3,double y1,double y2,double y3){
        double lenght1=Math.sqrt(Math.pow(Math.abs(x1-x2),2)+Math.pow(Math.abs(y1-y2),2));
        double lenght2=Math.sqrt(Math.pow(Math.abs(x1-x3),2)+Math.pow(Math.abs(y1-y3),2));
        double lenght3=Math.sqrt(Math.pow(Math.abs(x2-x3),2)+Math.pow(Math.abs(y2-y3),2));
        double squar=-1;
        if(lenght1+lenght2<lenght3 || lenght1+lenght3<lenght2 || lenght3+lenght3<lenght1 )
            System.out.println("нет треугольника");
        else {
        double polyperim=(lenght1+lenght2+lenght3)/2;
         squar=Math.sqrt(polyperim*(polyperim-lenght1)*(polyperim-lenght2)*(polyperim-lenght3));}
        return squar;
    }
    public void task2() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int a=in1.nextInt();
        int b=in1.nextInt();
        List<Integer> arrDelA=new ArrayList<>();
        List<Integer> arrDelB=new ArrayList<>();
        arrDelA.add(1);
        arrDelB.add(1);
        arrDelA=findDenominators(a,arrDelA);
        arrDelB=findDenominators(b,arrDelB);
        int GCF=GCF(findComm(arrDelA,arrDelB));
        System.out.println("НОД  "+GCF);
        System.out.println("НОK  "+GCM(a,b,GCF));

    }
    List<Integer> findDenominators(int a,List array){
      //  array.add(1);
        int i=2;
     while (a%i!=0){
            i++;
        }
        array.add(i);
    int starta=a;
     a=a/i;
     if(starta/i==1) {
         //array.add(i);
         return array;}
        findDenominators(a,array);
        return array;
    }
    List<Integer> findComm(List<Integer> arr1, List<Integer> arr2){
        List<Integer> arrCom=new ArrayList<>();
        for(int i=0;i<arr1.size();i++){

            int k=0;
            for(int j=0;j<arr2.size();j++){
                if(arr1.get(i).equals(arr2.get(j)) && k==0) {
                    arrCom.add(arr1.get(i));
                    arr1.set(i,1);
                    arr2.set(j,1);
                  //  arrCom.add(arr1.get(i));
                k++;}
            }
        }
        return arrCom;

    }
    int GCF(List<Integer> arrCom){
        int GCF=1;
        for (int k=0;k<arrCom.size();k++){
            GCF*=arrCom.get(k);
        }
        return GCF;
    }
    double GCM(int a,int b,int GCF){
        return (a*b)/GCF;
    }
    double GCM3(int a,int b,int c,int GCF){
        return (a*b*c)/GCF;
    }
    public void task3() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int a=in1.nextInt();
        int b=in1.nextInt();
        int c=in1.nextInt();
        int d=in1.nextInt();
        List<Integer> arrDelA=new ArrayList<>();
        List<Integer> arrDelB=new ArrayList<>();
        List<Integer> arrDelC=new ArrayList<>();
        List<Integer> arrDelD=new ArrayList<>();
        arrDelA.add(1);
        arrDelB.add(1);
        arrDelC.add(1);
        arrDelD.add(1);
        arrDelA=findDenominators(a,arrDelA);
        arrDelB=findDenominators(b,arrDelB);
        arrDelC=findDenominators(c,arrDelC);
        arrDelD=findDenominators(d,arrDelD);
        List<Integer> arrCom=findComm(findComm(arrDelA,arrDelB),findComm(arrDelC,arrDelD));
        int GCF=GCF(arrCom);
        System.out.println("НОД  "+GCF);
       // System.out.println("НОK  "+GCM(a,b,GCF));

    }




    public void task4() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int a=in1.nextInt();
        int b=in1.nextInt();
        int c=in1.nextInt();
        List<Integer> arrDelA=new ArrayList<>();
        List<Integer> arrDelB=new ArrayList<>();
        List<Integer> arrDelC=new ArrayList<>();
        arrDelA.add(1);
        arrDelB.add(1);
        arrDelC.add(1);
        arrDelA=findDenominators(a,arrDelA);
        arrDelB=findDenominators(b,arrDelB);
        arrDelC=findDenominators(c,arrDelC);
        List<Integer> arrCom=findComm(findComm(arrDelA,arrDelB),arrDelC);
        int GCF=GCF(arrCom);
        System.out.println("НОД  "+GCF);
        System.out.println("НОK  "+GCM3(a,b,c,GCF));

    }
    public void task5() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        double a=in1.nextDouble();
        double b=in1.nextDouble();
        double c=in1.nextDouble();
        List<Double> chisla=new ArrayList<>();
        chisla.add(a);
        chisla.add(b);
        chisla.add(c);
        System.out.println(findMax(chisla)+findMin(chisla));


    }
    double findMax(List<Double> arr){
        double max=Double.MIN_VALUE;
        for(int i=0;i<arr.size();i++){
            if(arr.get(i)>max) max=arr.get(i);
        }
        return max;
    }
    double findMin(List<Double> arr){
        double min=Double.MAX_VALUE;
        for(int i=0;i<arr.size();i++){
            if(arr.get(i)<min) min=arr.get(i);
        }
        return min;
    }
    public void task6() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        double x1 = in1.nextDouble();
        double x2 = in1.nextDouble();
        double x3 = in1.nextDouble();
        double x4 = in1.nextDouble();
        double x5 = in1.nextDouble();
        double x6 = in1.nextDouble();
        double y1 = in1.nextDouble();
        double y2 = in1.nextDouble();
        double y3 = in1.nextDouble();
        double y4 = in1.nextDouble();
        double y5 = in1.nextDouble();
        double y6 = in1.nextDouble();
        double square = space(x1, x2, x3, y1, y2, y3) + space(x1, x6, x3, y1, y6, y3) + space(x3, x4, x6, y3, y4, y6) + space(x4, x5, x6, y4, y5, y6);

        System.out.println(square);
    }
    public void task7() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int n=in1.nextInt();
        List<Double> arr=new ArrayList<>();
        for(int i=0;i<2*n;i++){
            arr.add(in1.nextDouble());
        }
        double Max=Double.MIN_VALUE;
        Dot maxDot1=new Dot(-111,-111);
        Dot maxDot2=new Dot(-111,-111);
        for(int i=0;i<n;i++){
            for (int j=i;j<n;j++){
                if(lenght(arr.get(i),arr.get(j),arr.get(i+n),arr.get(j+n))>Max){
                    Max=lenght(arr.get(i),arr.get(j),arr.get(i+n),arr.get(j+n));
                    maxDot1= new Dot(arr.get(i),arr.get(i+n));
                    maxDot2= new Dot(arr.get(j),arr.get(j+n));}



            }
        }
        maxDot1.show();
        maxDot2.show();
        System.out.println(Max);


    }
    class Dot{
        double x;
        double y;
        void show(){
            System.out.println("точка   "+"("+x+","+y+")");
        }
        Dot(double x, double y){
            this.x=x;
            this.y=y;
        }
    }
    public void task8() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int n=in1.nextInt();
        List<Double> arr=new ArrayList<>();
        for(int i=0;i<n;i++){
            arr.add(in1.nextDouble());
        }
        for(int i=0;i<n;i++){
            if(arr.get(i)==findMax(arr)){
                arr.set(i,Double.MIN_VALUE);
            }
        }
        System.out.println(findMax(arr));
    }
    public void task9() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int n=3;
        List<Double> arr=new ArrayList<>();
        for(int i=0;i<n;i++){
            arr.add(in1.nextDouble());
        }
        int k=0;
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(i!=j){
                    if(arr.get(i)%arr.get(j)==0)
                        k=1;
                }
            }
        }
        if(k==0)
            System.out.println("YES");
        else
            System.out.println("NO");
    }

    public void task10() {
        int sum=0;
       for(int i=1;i<=9;i++){
           if(i%2!=0)
               sum+=factorial(i);
       }
       System.out.println(sum);
    }
    public int factorial(int x) {
        while (x!=0)
        return x*factorial(x-1);
        return 1;
    }
    public void task11() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int n=in1.nextInt();
        List<Double> arr=new ArrayList<>();
        for(int i=0;i<n;i++){
            arr.add(in1.nextDouble());
        }
        System.out.println(sum3(2,arr));
        System.out.println(sum3(4,arr));
        System.out.println(sum3(5,arr));

    }
    public double sum3(int i,List<Double> arr){
        if(i-1>0 && i+1<arr.size())
        return arr.get(i-1)+arr.get(i)+arr.get(i+1);
        else{
            System.out.println("index out of range");
            return -1;
        }

    }
    public void task12() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        double x = in1.nextDouble();
        double y = in1.nextDouble();
        double z = in1.nextDouble();
        double t = in1.nextDouble();
        double square1=x*y/2;
        double lenght3=Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
        double square2=space(z,t,lenght3);
        System.out.println(square1+square2);


    }
    public double space(double lenght1,double lenght2,double lenght3){
        double squar=-1;
        if(lenght1+lenght2<lenght3 || lenght1+lenght3<lenght2 || lenght3+lenght3<lenght1 )
            System.out.println("нет треугольника");
        else {
            double polyperim=(lenght1+lenght2+lenght3)/2;
            squar=Math.sqrt(polyperim*(polyperim-lenght1)*(polyperim-lenght2)*(polyperim-lenght3));}
        return squar;
    }

    public void task13() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int n = in1.nextInt();
        for (int i=0;i<zifry(n).size();i++){
            System.out.println   ( zifry(n).get(i));

        }
    }
    List<Integer> zifry(int x){
        List<Integer> zifrRev=new ArrayList<>();
        List<Integer> zifr=new ArrayList<>();
        while(x/10>=1){
        zifrRev.add(x%10);
        x=x/10;}
        zifrRev.add(x%10);
        for (int i=zifrRev.size()-1;i>=0;i--){
           zifr.add(zifrRev.get(i));

        }
        return zifr;
    }
    public void task14() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int n = in1.nextInt();
        int m = in1.nextInt();
        if(zifry(n).size()>zifry(m).size()){
            System.out.println("the " +n+" is bigger");
        }
        else {
            if(zifry(n).size()==zifry(m).size()){
                System.out.println( n+" is same lenght as  "+m);
            }
            else System.out.println("the " +m+" is bigger");
        }

    }
    public void task15() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int k = in1.nextInt();
        int n = in1.nextInt();
        List<Integer> chisla=new ArrayList<>();
        for(int i=0;i<=n;i++){
    if(sumOfZifr(i)==k)
        chisla.add(i);
        }
        for(int i=0;i<chisla.size();i++){
            System.out.println(chisla.get(i));
        }


    }
    int sumOfZifr(int x){
        int sum=0;
        List<Integer> zifry=zifry(x);
        for(int i=0;i<zifry.size();i++){
            sum+=zifry.get(i);
        }
        return sum;
    }
    public void task16() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int n = in1.nextInt();
        List<Integer> arr= new ArrayList<>();
        for(int i =n;i<=2*n;i++){
            if(isSimple(i)) arr.add(i);
        }
        Collections.sort(arr);
        for(int i=0;i<arr.size()-1;i++){
            if(arr.get(i)+2== arr.get(i+1))
                System.out.println(arr.get(i)+" "+arr.get(i+1));
        }
    }
    boolean isSimple(int x){
        int k=0;
        for(int i=2;i<=x/2;i++){
            if(x%i==0){
                k++;
            }
        }
        if(k==0) return true;
        else return false;
    }
    public void task17() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int k = in1.nextInt();
        List<Integer> arrArm=new ArrayList<>();
        for(int i =1;i<=k;i++){
            if(Math.pow(sumOfZifr(i),zifry(i).size())==i) arrArm.add(i);
        }
        for(int i=0;i<arrArm.size();i++){
            System.out.println(arrArm.get(i));
        }
    }

    public void task18() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int n = in1.nextInt();
        int xFirst=(int)Math.pow(10,n-1);
        List<Integer> arr=new ArrayList<>();
        for (int i=xFirst;zifry(i).size()==n;i++){
            if(isaPos(zifry(i))){
                arr.add(i);
            }
        }
        for(int i=0;i<arr.size();i++){
            System.out.println(arr.get(i));
        }
    }
   boolean isaPos(List<Integer> arr){
      /* List<Integer> arr1=arr;
       Object[] arr2=new Object[arr.size()];
            arr2=arr.toArray();
       Collections.sort(arr);*/
      int k=0;
      for (int i=0;i<arr.size()-1;i++){
          if(arr.get(i)>=arr.get(i+1))
              k=1;
      }
        if(k==0){
            return true;
        }
        else return false;
    }
    public void task19() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int n = in1.nextInt();
        int xFirst=(int)Math.pow(10,n-1);
        List<Integer> arr=new ArrayList<>();
        for (int i=xFirst;zifry(i).size()==n;i++){
            if(isaNech(zifry(i))){
                arr.add(i);
            }
        }
       int sum=0;
        for(int i=0;i<arr.size();i++){
            sum+=arr.get(i);
        }
        System.out.println(sum);
    }
    boolean isaNech(List<Integer> arr){
        int k=0;
        for(int i=0;i<arr.size();i++){

            if(arr.get(i)%2==0)
                k=1;
        }
        if(k==0){
            return true;
        }
        else return false;
    }



    public void task20() {
        Scanner in1 = null;
        in1 = new Scanner(System.in);
        int x = in1.nextInt();
        int k=0;
        while (x!=0){
            x-=sumOfZifr(x);
            k++;
        }
        System.out.println(k);

    }

}
