import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

//import static com.sun.corba.se.impl.io.ObjectStreamClassCorbaExt.getDeclaredMethods;

public class MUltiArray {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Task task=new Task();
        Class<? extends Task> TaskClass=task.getClass();
        for (int i=1;i<=5;i++){
            System.out.println("Task "+i+"\n---------------------------");
            Method m=TaskClass.getMethod("task"+i);
            m.invoke(task);
            System.out.println("---------------------------");
        }
    }

}
class Task{
    public void task1() {
        int[][] MUlt = new int[3][4];
        for (int i = 0; i < 3; i++) {
            MUlt[i][0] = 1;
        }

        for (int i = 0; i < 3; i++) {
            for(int j = 0; j < 4; j++) {
                System.out.print(MUlt[i][j]+" ");
            }
            System.out.print("\n");
        }}
        public void task2() {
            double[][] MUlt = new double[2][3];
            for (int i = 0; i < 2; i++) {
                for(int j = 0; j < 3; j++){
                    MUlt[i][j]=Math.random()*9;
                    System.out.printf("%03.2f",MUlt[i][j]);
                    System.out.print(" ");
                }
                System.out.print("\n");
        }
    }
    public void task3() {
        int str=(int)(Math.random()*9)+1;
        int stol=(int)(Math.random()*9)+1;
        System.out.print(str+" "+ stol);
        System.out.print("\n");
        double[][] MUlt = new double[str][stol];
        for (int i = 0; i < str; i++) {
            for(int j = 0; j < stol; j++){
                MUlt[i][j]=Math.random()*9;
                System.out.printf("%03.2f",MUlt[i][j]);
                System.out.print(" ");
            }
            System.out.print("\n");
    }
        for (int i = 0; i < str; i++) {
            for(int j = 0; j < stol; j++){
                if(j==0 || j==stol-1){
                    System.out.printf("%03.2f",MUlt[i][j]);
                    System.out.print(" ");
                }

            }
            System.out.print("\n");}
    }
    public void task4() {
        int str=(int)(Math.random()*9)+1;
        int stol=(int)(Math.random()*9)+1;
        System.out.print(str+" "+ stol);
        System.out.print("\n");
        double[][] MUlt = new double[str][stol];
        for (int i = 0; i < str; i++) {
            for(int j = 0; j < stol; j++){
                MUlt[i][j]=Math.random()*9;
                System.out.printf("%03.2f",MUlt[i][j]);
                System.out.print(" ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
        for (int i = 0; i < str; i++) {
            if(i==0 || i==str-1){
            for(int j = 0; j < stol; j++){
                    System.out.printf("%03.2f",MUlt[i][j]);
                    System.out.print(" ");
                }
                System.out.print("\n");
            }}
    }

    public void task5() {
        int str=(int)(Math.random()*9)+1;
        int stol=(int)(Math.random()*9)+1;
        System.out.print(str+" "+ stol);
        System.out.print("\n");
        double[][] MUlt = new double[str][stol];
        for (int i = 0; i < str; i++) {
            for(int j = 0; j < stol; j++){
                MUlt[i][j]=Math.random()*9;
                System.out.printf("%03.2f",MUlt[i][j]);
                System.out.print(" ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
        for (int i = 0; i < str; i++) {
            if(i%2==0){
                for(int j = 0; j < stol; j++){
                    System.out.printf("%03.2f",MUlt[i][j]);
                    System.out.print(" ");
                }
                System.out.println();
            }

        }
    }

    }




