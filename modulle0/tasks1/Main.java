import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

    public class Main {
        public static void main(String[] args) throws FileNotFoundException {
            int n;


            Scanner in1 = new Scanner(System.in);

            n = in1.nextInt();
            switch (n) {
                case (1):
                    int x = in1.nextInt();
                    int y = in1.nextInt();

                    System.out.println("* = " + x * y);
                    System.out.println("- = " + (x - y));
                    System.out.println("+ = " + (x + y));
                    System.out.println("/ = " + x / y);
                    break;
                case (2):
                    int a = in1.nextInt();
                    System.out.println("c = " + (a+3));
                    break;
                case (3):
                    x = in1.nextInt();
                    y = in1.nextInt();
                    System.out.println("z = " + ((2 * x) + (y-2)*5));
                    break;
                case (4):
                    a = in1.nextInt();
                    int b= in1.nextInt();
                    int c= in1.nextInt();
                    System.out.println("z = " + (((a-3)*b/2)+c));
                    break;
                case (5):
                    x = in1.nextInt();
                    y = in1.nextInt();
                    System.out.println("среднее арифметическое = " + ((y+ x)/2));
                    break;
                case (6):
                    n= in1.nextInt();
                    System.out.println("m = " + (80/(80/n+12)));
                    break;
                case (7):
                    x= in1.nextInt();
                    System.out.println("S = " + (x*x*2));
                    break;
                case (8):
                    a = in1.nextInt();
                    b= in1.nextInt();
                    c= in1.nextInt();
                    System.out.println("f = " + ((b+Math.sqrt(Math.pow(b,2)+4*a*c))/(2*a)-Math.pow(a,3)*c+Math.pow(b,-2)));
                    break;
                case (9):
                    double a1 = in1.nextDouble();
                    double b1= in1.nextDouble();
                    double  c1= in1.nextDouble();
                    double d= in1.nextDouble();
                    System.out.println("f = " + ((a1/c1)*(b1/d)-(a1*b1-c1)/(c1*d)));
                    break;
                case (10):
                    double x1 = in1.nextDouble();
                    double y1 = in1.nextDouble();
                    System.out.println("f = " + (((Math.sin(x1)+Math.cos(y1))/(Math.cos(x1)-Math.sin(y1)))*Math.tan(x1*y1)));
                    break;
                case (11):
                    double i= in1.nextDouble();
                    double j= in1.nextDouble();
                    System.out.println("S = " + (j*i/2)+ "P="+ (i+j+(j/(Math.sin(Math.atan(j/i))))));
                    break;
                case (12):
                    x1= in1.nextDouble();
                    double x2= in1.nextDouble();
                    y1= in1.nextDouble();
                    double y2= in1.nextDouble();
                    if (y2>y1)
                    {i=y2-y1;}
                    else {i=y1-y2;}
                    if (x2>x1)
                    {j=x2-x1;}
                    else {j=x1-x2;}
                    System.out.println("расстояние ="+ ((j/(Math.sin(Math.atan(j/i))))));
                    break;
                case (13):
                    x1= in1.nextDouble();
                    x2= in1.nextDouble();
                    double x3= in1.nextDouble();
                    y2= in1.nextDouble();
                    double y3= in1.nextDouble();
                    if (y3>y2)
                    {i=y3-y2;}
                    else {i=y2-y3;}
                    if (x3>x1)
                    {j=x3-x1;}
                    else {j=x1-x3;}
                    double k;
                    if (x2>x3)
                    {k=x2-x3;}
                    else {k=x3-x2;}
                    if (x1<x2){
                        System.out.println("периметр ="+ (x2-x1+(k/(Math.sin(Math.atan(k/i))))+(j/(Math.sin(Math.atan(j/i))))));
                        System.out.println("площадь ="+ ((x2-x1)*i));}
                    if (x2<x1){
                        System.out.println("периметр ="+ (x1-x2+(k/(Math.sin(Math.atan(k/i))))+(j/(Math.sin(Math.atan(j/i))))));
                        System.out.println("площадь ="+ ((x1-x2)*i));}
                    break;
                case (14):
                    double r= in1.nextDouble();
                    System.out.println("S = " + (Math.PI*Math.pow(r,2))+ "P="+ (Math.PI*2*r));
                    break;
                case (15):
                    System.out.println("1 степень  "+ Math.pow(Math.PI,1)+"    2 степень  "+ Math.pow(Math.PI,2)+"   3 степень  "+ Math.pow(Math.PI,3)+"  4 степень  "+ Math.pow(Math.PI,4));
                    break;
                case (16):
                    System.out.println("4значное число");
                    x=in1.nextInt();
                    System.out.println((x%10)*(x/10%10)*(x/100%10)*(x/1000%10));
                    break;
                case (17):
                    x1= in1.nextDouble();
                    x2= in1.nextDouble();
                    System.out.println("среднее арифметическое 2 кубов  "+ ((Math.pow(x1,3)+Math.pow(x2,3))/2));
                    System.out.println("среднее геометрическое "+ (Math.sqrt(Math.abs(x1)*Math.abs(x2))));
                    break;
                case (18):
                    x1= in1.nextDouble();
                    System.out.println("площадь грани "+ (x1*x1));
                    System.out.println("площадь поверхности "+ (x1*x1*6));
                    System.out.println("объем "+ (x1*x1*x1));
                    break;
                case (19):
                    x1= in1.nextDouble();
                    System.out.println("высота  "+ (x1*3/Math.sqrt(2)));
                    System.out.println("радиус вписанного "+ (x1*3/Math.sqrt(6)));
                    System.out.println("радиус описанного  "+ (x1*3/Math.sqrt(3)));

                    break;
                case (20):
                    x1= in1.nextDouble();
                    r=x1/Math.PI*3;
                    System.out.println("площадь"+ x1/2*r);
                    break;
                case (21):
                    x1= in1.nextDouble();
                    int zel=(int)x1;
                    double drob=(x1-zel)*1000;
                    int yu=(int)drob;
                    if (drob*10%10>=5)
                        yu=yu+1;
                    System.out.println(yu+","+zel);
                case (22):
                    x1= in1.nextDouble();
                    double sec = x1%60;
                    double min= (x1-sec)%60;


                  //  System.out.println(yu+","+zel);

            }
        }
    }

