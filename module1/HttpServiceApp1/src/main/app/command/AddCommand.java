package main.app.command;

import main.app.entity.TravelVoucherType;
import main.app.service.TravelVoucherService;
import main.app.service.SimpleValidatorPost;
import main.app.service.ValidatorPost;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import java.util.Map;

 public class AddCommand implements AppCommand {
    public String name = "ADD";

    public String getName() {
        return name;
    }
    TravelVoucherService travelVoucherService;
    ValidatorPost simpleValidatorPost=new SimpleValidatorPost();
   // Logger logger= LogManager.getLogger();
    public AddCommand(TravelVoucherService travelVoucherService) {
        this.travelVoucherService = travelVoucherService;
    }
    @Override
    public String process(Map<String,String> requestGetMap) {
     //   logger.info("add_command_start");
        String id = requestGetMap.get("id");
        String name = requestGetMap.get("name");
        String type = requestGetMap.get("type");
           Long i=Long.parseLong(id);
            if(simpleValidatorPost.validate(i,name,type)){
                this.travelVoucherService.addVoucher(Long.parseLong(id),name,TravelVoucherType.valueOf(type.toUpperCase()));
          //      logger.info("add_command_success");
                return "IS_ADDED_NOW";
            }
            else
                //logger.info("add_command_failed");
                return "INVALID_PARAMS";
            }
    }



