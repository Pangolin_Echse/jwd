package main.app.command;
import main.app.service.TravelVoucherService;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import java.util.Map;

public class DeleteCommand implements AppCommand {
    public String name = "DELETE";
    public String getName() {
        return name;
    }
    TravelVoucherService travelVoucherService;
   // Logger logger= LogManager.getLogger();
    public DeleteCommand(TravelVoucherService travelVoucherService) {
        this.travelVoucherService = travelVoucherService;
    }

    @Override
    public String process(Map<String,String> requestGetMap) {
     // logger.info("delete_command_start");
        String id = requestGetMap.get("id");
        if(id!=null) {
            boolean result = this.travelVoucherService.deleteVoucher(Long.parseLong(id));
            if(result){
           //     logger.info("delete_command_success");
                return "IS_DELETED_NOW";
            }
            else{
              //  logger.info("delete_command_failed");
                return "NO_SUCH_DATA";
            }
        }
      //  logger.info("delete_command_failed");
        return "NO_SUCH_DATA";
    }
}

