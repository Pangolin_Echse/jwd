package main.app.command;

import java.util.Map;

public interface AppCommand {

    String process(Map<String, String> params);
    public String getName();
}
