package main.app.command;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import java.util.Map;



public class SimpleCommandMaker implements CommandMaker {
    Map<String, AppCommand> commandStringMap;
    //Logger logger= LogManager.getLogger();
    public SimpleCommandMaker(Map<String,AppCommand> commandStringMap) {
        this.commandStringMap = commandStringMap;
    }

    @Override
    /////////////// try to do pattern command
    public AppCommand makeCommand(String commandName) {
        return commandStringMap.get(commandName.toLowerCase());
    }
    @Override
    //////////////try to do  pattern facade
    public AppCommand findCommandName( Map<String, String> getParams){
        AppCommand command= null;
        try {
            String commandName = getParams.get("command");
            command = makeCommand(commandName);
        }
        catch (Exception e){
         //   logger.error(e);
        }
        return command;
    }
}