package main.app.command;

import main.app.command.AppCommand;

import java.util.Map;

public interface CommandMaker {
    public AppCommand makeCommand(String commandName);
    public AppCommand findCommandName( Map<String, String> getParams);
}