package main.app.command;

import main.app.ApplicationConstants;
import main.app.entity.TravelVoucher;
import main.app.service.TravelVoucherService;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import java.util.Map;

public class ShowCommand implements AppCommand {
    public String name = "SHOW";
    TravelVoucherService travelVoucherService;
    //Logger logger= LogManager.getLogger();

    public String getName() {
        return name;
    }

    public ShowCommand(TravelVoucherService travelVoucherService) {
        this.travelVoucherService = travelVoucherService;
    }

    @Override
    public String process(Map<String, String> requestGetMap) {
       // logger.info("start_command_show");
        StringBuffer stringBuffer = new StringBuffer();
        Map<Long,TravelVoucher> travelVouchers=travelVoucherService.findAllTravelVouchers();
        for(Map.Entry<Long,TravelVoucher> travelVoucher:travelVouchers.entrySet()){
            stringBuffer.append("<form method=\"POST\">"+travelVoucher.getValue().getName()+"<input type='hidden' name='id' value='" +travelVoucher.getKey()+
                    "'/>"+"<input type='hidden' name='" + ApplicationConstants.COMMAND_PARAM+
                    "' value='delete'"+travelVoucher.getKey()+"'/>"+"<input type='submit' value='DELETE'/>"+"</form>");
        }
        stringBuffer.append("<form method=\"POST\">" +
                "<input type=\"text\" name=\"name\" value=\"\" placeholder=\"Name\" required  minlength=\"1\">" +
                "<input type=\"number\" name=\"id\" value=\"\" placeholder=\"ID\" required >" +
                "<input type=\"text\" name=\"type\" value=\"\" placeholder=\"TYPE\" required  minlength=\"1\">" +
                "<input type=\"hidden\" name=\"command\" value=\"add\">" +
                "<input type=\"submit\" value=\"ADD\"></form>\n");

        return stringBuffer.toString();
    }
}
