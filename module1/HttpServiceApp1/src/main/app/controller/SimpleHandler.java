package main.app.controller;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import main.app.command.CommandMaker;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.*;

public class SimpleHandler implements HttpHandler {
    private CommandMaker commandMaker;
   // Logger logger= LogManager.getLogger();
    public SimpleHandler(CommandMaker commandMaker) {
        this.commandMaker = commandMaker;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
       // logger.info(httpExchange.getRequestURI());
        Map<String, String> getParams = parsPostRequest(httpExchange);
        String templateView = new String(Files.readAllBytes(Paths.get("./src/main/resources/defaultTemplate.html")), StandardCharsets.UTF_8);
        String returnData="";
        String name = "";
        if( commandMaker.findCommandName(getParams)!=null) {
            returnData = commandMaker.findCommandName(getParams).process(getParams);
            name = commandMaker.findCommandName(getParams).getName();
        }
        String content = commandMaker.makeCommand("SHOW").process(getParams);
        String view = MessageFormat.format(templateView, name, returnData, content);
        httpExchange.sendResponseHeaders(200, view.length());
        OutputStream responseBody = httpExchange.getResponseBody();
        responseBody.write(view.getBytes());
        responseBody.flush();
        responseBody.close();
    }


    private Map<String, String> parsPostRequest(HttpExchange httpExchange) {
        InputStream inputStream = httpExchange.getRequestBody();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuffer buffer = new StringBuffer();
        Scanner bufferScan= new Scanner(bufferedReader);
        while (bufferScan.hasNextLine()) {
            buffer.append(bufferScan.nextLine());
        }
        System.out.println(buffer);
        return parsing(buffer);
    }
    private Map<String, String> parsing(StringBuffer buffer) {
        Map<String, String> stringMap = new HashMap<>();
        StringTokenizer st= new StringTokenizer(buffer.toString(),"&,=");
        int n=st.countTokens()/2;
        for(int i=0;i<n;i++){
            stringMap.put(st.nextToken(), st.nextToken());
        }
        return stringMap;
    }
}