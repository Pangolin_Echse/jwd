package main.app;
import com.sun.net.httpserver.HttpServer;
import main.app.command.CommandMaker;
import main.app.command.SimpleCommandMaker;
import main.app.controller.SimpleHandler;
import main.app.service.*;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import java.io.*;
import java.net.InetSocketAddress;


public class HttpServerApp1 {
    public static void main(String[] args) throws IOException {
  //      Logger logger= LogManager.getLogger(HttpServerApp1.class);
        final InetSocketAddress inetSocketAddress = new InetSocketAddress(ApplicationConstants.url, ApplicationConstants.port);
        HttpServer server = HttpServer.create(inetSocketAddress, 0);
        TravelVoucherService travelVoucherService = new SimpleTravelVoucherService();
        CommandMapFactory commands=new CommandMapFactory();
        CommandMaker commandMaker = new SimpleCommandMaker(commands.buildMap(travelVoucherService));
        server.createContext("/chill", new SimpleHandler(commandMaker));
        server.start();
   //     logger.info("Server started on http://" + ApplicationConstants.url + ":" + ApplicationConstants.port + "/chill");
    }
}
