package main.app.dao;
import main.app.entity.TravelVoucher;
import main.app.entity.TravelVoucherType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class  Data {

    Map<Long,TravelVoucher> travelVouchers =  new HashMap<>();
    {
        for(int i=1;i<10;i++){
            if(i%2==0){
                travelVouchers.put(Long.valueOf(i),new TravelVoucher("voucher"+i, TravelVoucherType.CHILL));
            }
            if(i==1){
                travelVouchers.put(Long.valueOf(i),new TravelVoucher("voucher"+i, TravelVoucherType.CRUISE));
            }
            if(i==3){
                travelVouchers.put(Long.valueOf(i),new TravelVoucher("voucher"+i, TravelVoucherType.EXCURSION));
            }
            if(i==5){
                travelVouchers.put(Long.valueOf(i),new TravelVoucher("voucher"+i, TravelVoucherType.TREATMENT));
            }
            if(i==7 || i==9){
                travelVouchers.put(Long.valueOf(i),new TravelVoucher("voucher"+i, TravelVoucherType.SHOPPING));
            }
        }
    }


    public Map<Long,TravelVoucher>  getData() {
        return travelVouchers;
    }

}

