package main.app.entity;

public class TravelVoucher {
    private Long id;
    private String name;
    private TravelVoucherType type;

    public TravelVoucher( String name, TravelVoucherType type) {
        this.name = name;
        this.type = type;
    }


    public String getName() {
        return name;
    }

    public TravelVoucherType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "TravelVoucher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                '}';

    }

}

