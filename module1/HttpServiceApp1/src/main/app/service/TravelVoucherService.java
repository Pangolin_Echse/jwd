package main.app.service;

import main.app.entity.TravelVoucher;
import main.app.entity.TravelVoucherType;

import java.util.Map;

public interface TravelVoucherService {

    Map<Long,TravelVoucher> findAllTravelVouchers();
    boolean deleteVoucher(Long id);
    void addVoucher(Long id, String name, TravelVoucherType type);
}

