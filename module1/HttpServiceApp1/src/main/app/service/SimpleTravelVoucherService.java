package main.app.service;
import main.app.dao.Data;
import main.app.entity.TravelVoucher;
import main.app.entity.TravelVoucherType;

import java.util.*;

public class SimpleTravelVoucherService implements TravelVoucherService {

    Data data=new Data();
    public SimpleTravelVoucherService(){
        this.data=data;
    }

    @Override
    public Map<Long,TravelVoucher> findAllTravelVouchers() {
        return data.getData();
    }
    @Override
    public boolean deleteVoucher(Long id){
        if(data.getData().containsKey(id)){
                data.getData().remove(id);
                return  true;
        }
        else    return false;
    }
    @Override
    public void addVoucher(Long id, String name, TravelVoucherType type){
        data.getData().put(id,new TravelVoucher( name, type));
    }

}










