package main.app.service;

import main.app.command.AddCommand;
import main.app.command.AppCommand;
import main.app.command.DeleteCommand;
import main.app.command.ShowCommand;

import java.util.HashMap;
import java.util.Map;
//////////////try to do  pattern factory
public class CommandMapFactory {
    Map<String, AppCommand> commands=new HashMap<>();
    public Map<String, AppCommand> buildMap( TravelVoucherService travelVoucherService ){
        commands.put("show",new ShowCommand(travelVoucherService));
        commands.put("delete",new DeleteCommand(travelVoucherService));
        commands.put("add",new AddCommand(travelVoucherService));
        return commands;
    }
}
