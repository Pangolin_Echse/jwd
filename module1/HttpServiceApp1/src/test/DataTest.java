package test;

import main.app.dao.Data;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.JUnit4;

import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;


@RunWith(JUnit4.class)
public class DataTest {

    private Data data;

    @Before
    public void initTest() {
        data = new Data();
    }

    @After
    public void afterTest() {
        data = null;
    }

    @Test
    public void testGetData() {
        assertEquals(9, data.getData().size());
    }


}

