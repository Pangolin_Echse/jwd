
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <title>JSP Application</title>
</head>
<body>
<h2>Available commands</h2>
<form action="${pageContext.request.contextPath}/dom" method="post">
  <input type="text" name="path"/>
  <button type="submit">DOM</button>
</form>
<form action="${pageContext.request.contextPath}/stax" method="post">
  <input type="text" name="path"/>
  <button type="submit">STAX</button>
</form>
<form action="${pageContext.request.contextPath}/sax" method="post">
  <input type="text" name="path"/>
  <button type="submit">SAX</button>
</form>

<h2>Answer</h2>
${returndata}
<h2>Current data</h2>

<c:forEach var="tarif" items="${showdata}">
  <br/>
  <c:out value="${tarif}"/>
</c:forEach>


</body>
</html>
