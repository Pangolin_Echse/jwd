package by.training.app.controller;

import by.training.app.bean.Device;
import by.training.app.services.DeviceService;
import by.training.app.services.SimpleDeviceServer;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class SaxServlet extends HttpServlet {
    SimpleDeviceServer parseService= new SimpleDeviceServer();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path=req.getParameter("path");
        try {
            parseService.parseWithSax(path);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException | XMLStreamException e) {
            e.printStackTrace();
        }
        resp.getWriter().write(parseService.showDeviceData());

    }
}
