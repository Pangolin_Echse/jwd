package by.training.app.dao;

import by.training.app.bean.Device;

import java.util.ArrayList;
import java.util.List;

public class Data {
   static List<Device> data= new ArrayList<Device>();

    public Data() {

    }

    public Data(List<Device> data) {
        this.data = data;
    }


    public List<Device> getData() {
        return this.data;
    }


    public void setData(List<Device> data) {
        this.data = data;
    }

    public boolean delete(long id) {
        return false;
    }

    public void deleteAll() {
        data.clear();
    }

    public void addDevice(Device device) {
        this.data.add(device);
    }
}


