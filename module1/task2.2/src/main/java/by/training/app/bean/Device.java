package by.training.app.bean;

import javax.xml.bind.annotation.XmlType;
import java.util.List;

public class Device {
    private String name;
    private String id;
    private String origin;
    private String prise;
    private String critical;
    private Type type;


    public Device() {
    }


    public Device(String name, String id,  String origin,String prise,String critical,Type type)
    {
        this.name = name;
        this.id = id;
        this.origin = origin;
        this.prise = prise;
        this.critical = critical;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Device{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", origin=" + origin +
                ", prise=" + prise +
                ", critical=" + critical +
                ", type=" + type.toString() +
                "}";
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
    public String getPrise() {
        return prise;
    }

    public void setPrise(String prise) {
        this.prise = prise;
    }
    public String getCritical() {
        return critical;
    }

    public void setCritical(String critical) {
        this.critical = critical;
    }
    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public static class Builder {
        private Device newDevice;

        public Builder() {
            newDevice = new Device();
        }

        public Builder setName(String name) {
            newDevice.name = name;
            return this;
        }

        public Builder setId(String id) {
            newDevice.id = id;
            return this;
        }

        public Builder setOrigin(String origin) {
            newDevice.origin = origin;
            return this;
        }

        public Builder setPrise(String prise) {
           newDevice.prise = prise;
            return this;
        }
        public Builder setCritical(String critical) {
            newDevice.critical = critical;
            return this;
        }

        public Builder setType( String periphery,String energyConsumption,String cooler,String group,List<String>ports) {
            newDevice.type = new Type(periphery,energyConsumption,cooler,group,ports);
            return this;
        }
        public Builder setType(Type type) {
            newDevice.type = type;
            return this;
        }
        public Device build() {
            return newDevice;
        }
    }

}

