package by.training.app.services;

import by.training.app.bean.Device;
import by.training.app.dao.Data;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public interface DeviceService {
    boolean parseWithDom(String path) throws ParserConfigurationException, IOException, SAXException;

    String parseWithStax(String path) throws XMLStreamException, ParserConfigurationException, SAXException, IOException;

    Data getData();
 void parseWithSax(String path) throws XMLStreamException, ParserConfigurationException, SAXException, IOException ;

    char[] showDeviceData();

    void deleteAllData();

    void addDevice(Device device);
}
