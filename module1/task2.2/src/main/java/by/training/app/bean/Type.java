package by.training.app.bean;

import java.util.ArrayList;
import java.util.List;

public class Type {
    private String periphery;
    private String energyConsumption;
    private String cooler;
    private  String group;
    private List<String> ports;

    public Type() {
    }


    public Type(String periphery, String energyConsumption,  String cooler,String group,List<String> ports)
    {
        this.periphery = periphery;
        this.energyConsumption= energyConsumption;
        this.cooler = cooler;
        this.group = group;
        this.ports = ports;
    }

    @Override
    public String toString() {
        return "Type{" +
                "periphery='" + periphery + '\'' +
                ", energyConsumption='" + energyConsumption + '\'' +
                ", cooler='" + cooler + '\'' +
                ", group='" + group + '\'' +
                ", ports=" + ports +
                '}';
    }


    public String getPeriphery() {
        return periphery;
    }

    public void setPeriphery(String periphery) {
        this.periphery = periphery;
    }
    public String getEnergyConsumption() {
        return energyConsumption;
    }

    public void setEnergyConsumption(String energyConsumption) {
        this.energyConsumption = energyConsumption;
    }
    public String getCooler() {
        return cooler;
    }

    public void setCooler(String cooler) {
        this.cooler = cooler;
    }
    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
    public String getPorts() {
        return periphery;
    }

    public void setPorts(List<String> ports) {
        this.ports = ports;
    }
}
