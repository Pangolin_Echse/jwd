package by.training.app.services;

import by.training.app.bean.Device;
import by.training.app.bean.Type;
import by.training.app.dao.Data;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SaxHandler extends DefaultHandler {
   // static final Logger LOGGER = AppConstants.COMMANDS_LOGGER;

    DeviceService deviceService;
    Device.Builder builder;
    List<String> ports;
    String lastElementName;
    Type type;

    String content = null;
    public SaxHandler(DeviceService deviceService) {
        this.deviceService =deviceService;
    }

   // @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        lastElementName = qName;

        if (qName.equals("Device")) {
            try {
                builder = new Device.Builder();
                builder.setId(attributes.getValue("id"));
                builder.setName(attributes.getValue("name"));
                builder.setOrigin(attributes.getValue("origin"));
                builder.setPrise(attributes.getValue("price"));
                builder.setCritical(attributes.getValue("critical"));
            } catch (Exception e) {
               // LOGGER.error("sax error", e);
            }
        }
        if (qName.equals("ports")) {
           ports=new ArrayList<>();
        }
        if (qName.equals("Type")) {
           type=new Type() ;
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        if (qName.equals("Device")) {
            deviceService.addDevice(builder.build());
        }
        if (qName.equals("Type")) {
            builder.setType(type);
        }
        if (qName.equals("perephery")) {
           type.setPeriphery(content);
        }
        if (qName.equals("energyConsumpion")) {
            type.setEnergyConsumption(content);
        }
        if (qName.equals("cooler")) {
            type.setCooler(content);
        }
        if (qName.equals("group")) {
            type.setGroup(content);
        }
        if (qName.equals("name")) {
           ports.add(content);
        }
        if (qName.equals("ports")) {
           type.setPorts(ports);
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        content = String.copyValueOf(ch, start, length).trim();
    }
}



