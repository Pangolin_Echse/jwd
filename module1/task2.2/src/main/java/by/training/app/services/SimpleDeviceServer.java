package by.training.app.services;

import by.training.app.bean.Device;
import by.training.app.bean.Type;
import by.training.app.dao.Data;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleDeviceServer implements DeviceService {
    private static Data data=new Data();

    public  SimpleDeviceServer(Data data) {
        this.data = data;
    }
    public  SimpleDeviceServer(){}
    public Data getData() {
        return data;
    }



    @Override
    public void deleteAllData() {

    }

  
   
    @Override

    public char[] showDeviceData() {
        StringBuilder stringBuilder=new StringBuilder();
        for(Device device: data.getData()){
            stringBuilder.append(device.toString());
        }
        return stringBuilder.toString().toCharArray();

    }
  @Override
    public void addDevice(Device device) {
        data.addDevice(device);
    }

    public void deleteAll() {
        data.deleteAll();
    }
    public void parseWithSax(String path) throws XMLStreamException, ParserConfigurationException, SAXException, IOException {
 
        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
        SAXParser parser = parserFactor.newSAXParser();
        SaxHandler handler = new SaxHandler(this);
        parser.parse(new File(path),handler);
    }



    @Override
    public String parseWithStax(String path) throws XMLStreamException {
    
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader;
        List<String> ports = new ArrayList<String>();
        Type type=new Type();
        String name = null;
        String id;
         String origin;
         String price;
        String critical;
        Device.Builder builder = null;

        try {
            reader = factory.createXMLStreamReader( new FileInputStream(path));


            while (reader.hasNext()) {
                int event = reader.next();
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        switch (reader.getLocalName()) {
                            case "Device":
                                builder = new Device.Builder();
                                builder.setId(reader.getAttributeValue("", "id"));
                                builder.setOrigin(reader.getAttributeValue("", "origin"));
                                builder.setPrise(reader.getAttributeValue("", "price"));
                                builder.setName(reader.getAttributeValue("", "name"));
                                builder.setCritical(reader.getAttributeValue("", "critical"));
                            case "Type":
                                type = new Type();
                                break;
                            case "ports":
                                ports = new ArrayList<String>();
                                break;
                            case "perephery":
                                type.setPeriphery(reader.getElementText());
                                break;
                            case "energyConsumption":
                                type.setEnergyConsumption(reader.getElementText());
                                break;
                            case "cooler":
                                type.setCooler(reader.getElementText());
                                break;
                            case "group":
                                type.setGroup(reader.getElementText());
                                break;
                                case "name":
                                ports.add(reader.getElementText());
                                break;
                        }
                            case XMLStreamConstants.END_ELEMENT:
                        switch (reader.getLocalName()) {
                            case "Device":
                                data.addDevice(builder.build());
                                break;
                            case "Type":
                                builder.setType(type);
                                break;
                            case "ports":
                               type.setPorts(ports);
                                break;

                        }
                        break;
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
         //   LOGGER.error("reader XMLStreamException error", e);
            return "reader XMLStreamException in add stax";

        }
        
        return null;
    }

    @Override
    public boolean parseWithDom(String path) throws ParserConfigurationException, IOException, SAXException {
    //    LOGGER.info("domToData Start");
      
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            // Получили из фабрики билдер, который парсит XML, создает структуру Document в виде иерархического дерева.
            DocumentBuilder builder1 = factory.newDocumentBuilder();

            // Запарсили XML, создав структуру Document. Теперь у нас есть доступ ко всем элементам, каким нам нужно.
            Document document = builder1.parse(new File(path));


            NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node instanceof Element) {
                Device.Builder builder = new Device.Builder();
                builder.setId(node.getAttributes().getNamedItem("id").getNodeValue());
                builder.setName(node.getAttributes().getNamedItem("name").getNodeValue());
                builder.setOrigin(node.getAttributes().getNamedItem("origin").getNodeValue());
                builder.setPrise(node.getAttributes().getNamedItem("price").getNodeValue());
                builder.setCritical(node.getAttributes().getNamedItem("critical").getNodeValue());
                try {
                    NodeList type = node.getChildNodes();
                    NodeList params=type.item(0).getChildNodes();
                    String periphery="";
                    String energyConsumption="";
                    String cooler="";
                    String group="";
                    List<String> ports=new ArrayList<>();

                    //  NodeList childNodes;
                    for (int j = 0; j < params.getLength(); j++) {
                        switch (params.item(j).getNodeName()) {
                            case "perephery":
                                periphery=params.item(j).getNodeValue();

                                break;
                            case "energyConsumption":
                                energyConsumption=params.item(j).getNodeValue();
                                break;
                            case "cooler":
                                cooler=params.item(j).getNodeValue();
                                break;
                            case "group":
                                group=params.item(j).getNodeValue();
                                break;
                            case "ports":
                               NodeList names = params.item(j).getChildNodes();
                                for (int k = 0; k < names.getLength(); k++) {
                                    ports.add(names.item(k).getNodeValue());
                                }
                                break;

                            default:
                               // LOGGER.info("continue");

                        }
                    }
                    builder.setType(periphery,energyConsumption,cooler,group,ports);


                } catch (Exception e) {
                  //  LOGGER.error("cant parse", e);
                }
                data.addDevice(builder.build());

            }
        }
        //LOGGER.info(getTariffsData().getTariffsListFromDAL());
        return true;
    }


}


