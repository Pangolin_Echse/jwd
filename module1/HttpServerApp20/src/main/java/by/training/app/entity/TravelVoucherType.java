package by.training.app.entity;

public enum TravelVoucherType {

    CHILL,
    EXCURSION,
    TREATMENT ,
    SHOPPING,
    CRUISE
}

