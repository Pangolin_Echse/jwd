package by.training.app.command;

import by.training.app.service.ComparatorMapFactory;
import by.training.app.service.TravelVoucherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class SortCommand implements AppCommand {
    private String name = "SORT";
    TravelVoucherService travelVoucherService;
    ComparatorMapFactory comparatorMapFactory;
    Logger logger = LogManager.getLogger();
    public TravelVoucherService getTravelVoucherService() {
        return travelVoucherService;
    }
    public String getName() {
        return name;
    }

    public SortCommand(TravelVoucherService travelVoucherService, ComparatorMapFactory comparatorMapFactory) {
        this.travelVoucherService = travelVoucherService;
        this.comparatorMapFactory=comparatorMapFactory;
    }

    @Override
    public String process(Map<String, String> requestGetMap) {
        String characters = requestGetMap.get("characters");
        logger.info("sort_command_start");
        return validate(characters);
    }
   private String validate(String s){
        if("cost".equals(s)){
            logger.info("Cost sort execute");
            this.travelVoucherService.sortVouchers("cost",comparatorMapFactory.buildMap().get(s));
            return " Now data is sorted";
        }
        else{
            logger.info("Invalid type of sort. Default sort execute");
            this.travelVoucherService.sortVouchers("cost",comparatorMapFactory.buildMap().get("default"));
            return "You can sort data only by cost. Now sort is default by name";
        }
   }

}
