package by.training.app.service;

import by.training.app.command.*;
import java.util.HashMap;
import java.util.Map;

//////////////try to do  pattern factory
public class CommandMapFactory {
    Map<String, AppCommand> commands = new HashMap<>();

    public Map<String, AppCommand> buildMap(TravelVoucherService travelVoucherService, FileService fileService, ValidatorPost validatorPost,ComparatorMapFactory comparatorMapFactory) {
        commands.put("show", new ShowCommand(travelVoucherService));
        commands.put("delete", new DeleteCommand(travelVoucherService));
        commands.put("add", new AddCommand(travelVoucherService, validatorPost));
        commands.put("getfilename", new LoadFileCommand(travelVoucherService, fileService));
        commands.put("default", new DefaultCommand());
        commands.put("showc", new ShowCCommand());
        commands.put("sort", new SortCommand(travelVoucherService,comparatorMapFactory));
        commands.put("filtr", new FiltrCommand(travelVoucherService));
        return commands;
    }
}
