package by.training.app.service;

import by.training.app.entity.TravelVoucher;
import by.training.app.entity.TravelVoucherType;


import java.util.Comparator;
import java.util.Map;

public interface TravelVoucherService {

    Map<Long, TravelVoucher> findAllTravelVouchers();
    boolean deleteVoucher(Long id);
    void addVoucher(Long id, String name,int cost,int people, TravelVoucherType type);
    void sortVouchers(String byType, Comparator<? super Map.Entry<Long, TravelVoucher>> c);
    Map<Long, TravelVoucher> filtr();
}

