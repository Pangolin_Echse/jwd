package by.training.app.service;

import by.training.app.command.*;
import by.training.app.entity.TravelVoucher;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class ComparatorMapFactory {
    Map<String,Comparator<? super Map.Entry<Long, TravelVoucher>> > comparators= new HashMap<>();

    public Map<String, Comparator<? super Map.Entry<Long, TravelVoucher>> > buildMap() {
        comparators.put("cost",  Comparator.comparingInt((Map.Entry<Long, TravelVoucher> o) -> o.getValue().getCost()));
        comparators.put("default", Comparator.comparing((Map.Entry<Long, TravelVoucher> o) -> o.getValue().getName().toLowerCase()).thenComparingInt((Map.Entry<Long, TravelVoucher> o) -> o.getValue().getCost()));
        return comparators;
    }
}
