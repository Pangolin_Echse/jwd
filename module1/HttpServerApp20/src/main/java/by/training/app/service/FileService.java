package by.training.app.service;


import by.training.app.entity.TravelVoucherType;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class
FileService {
    Path path;
    TravelVoucherService travelVoucherService;

    public List<Integer> readFile(Path path, TravelVoucherService travelVoucherService) throws IOException {

            this.path = path;
            this.travelVoucherService = travelVoucherService;
            List<Integer> wrongLines = new ArrayList<>();
        try (BufferedReader buffer = Files.newBufferedReader(path)) {
            String st = buffer.readLine();
            if (st.matches("([^,]+),([^,]+),([^,]+),([^,]+),([^,]+)")) {
                long lineCount = Files.lines(path).count();
                int i = 0;
                while (i < lineCount - 1) {
                    st = buffer.readLine();
                    if (st.matches("(\\d+),([^,]+),(\\d+),(\\d+),(CHILL)|(\\d+),([^,]+),(\\d+),(\\d+),(CRUISE)")) {
                        StringTokenizer str = new StringTokenizer(st, ",");
                        travelVoucherService.addVoucher(Long.valueOf(str.nextToken()), str.nextToken(), Integer.parseInt(str.nextToken()), Integer.parseInt(str.nextToken()), TravelVoucherType.valueOf(str.nextToken()));
                    } else wrongLines.add(i + 1);
                    i++;
                }
            } else wrongLines.add(0);
            return wrongLines;
        }

    }
}
