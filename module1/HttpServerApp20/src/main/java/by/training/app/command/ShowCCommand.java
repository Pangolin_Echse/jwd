package by.training.app.command;

import by.training.app.service.TravelVoucherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class ShowCCommand implements AppCommand {
    private String name = "SHOW";
    Logger logger= LogManager.getLogger();
    TravelVoucherService travelVoucherService;
    public String getName() {
        return name;
    }
    public TravelVoucherService getTravelVoucherService() {
        return travelVoucherService;
    }

    @Override
    public String process(Map<String, String> requestGetMap)
    {
        logger.info("command showC is running");
        return "NOW YOU CAN SEE ME";
    }
}
