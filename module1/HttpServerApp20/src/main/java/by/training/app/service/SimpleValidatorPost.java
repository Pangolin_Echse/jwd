package by.training.app.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import by.training.app.dao.Data;
import by.training.app.entity.TravelVoucherType;

public class SimpleValidatorPost implements ValidatorPost {
     Data data;
     Logger logger= LogManager.getLogger();

     public SimpleValidatorPost(Data data) {
       this.data=data;

     }

    @Override
    public String validate(String id, String name, String type )
    {
        Long id1=Long.valueOf(id);
        TravelVoucherType[] test= TravelVoucherType.values();
        if( !data.getData().containsKey(id1)){
            for (TravelVoucherType travelVoucherType : test) {
                if (type.equalsIgnoreCase(travelVoucherType.toString())) {
                    logger.info("Client input is correct");
                    return "true";
                }}

                    logger.error("No such type in Enum");
                    return "THERE IS NOT SUCH TYPE";

            }

        logger.error("Already exist item with the same key");
        return "YOU ALREADY HAVE ITEM WITH SAME KEY";

    }
}
