package by.training.app.command;

import by.training.app.ApplicationConstants;
import by.training.app.entity.TravelVoucher;
import by.training.app.service.TravelVoucherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class FiltrCommand implements AppCommand {
    TravelVoucherService travelVoucherService;
    Logger logger= LogManager.getLogger();
    String name = "FILTR";

    public TravelVoucherService getTravelVoucherService() {
        return travelVoucherService;
    }
    public String getName() {
        return name;
    }

    public FiltrCommand(TravelVoucherService travelVoucherService) {
        this.travelVoucherService = travelVoucherService;
    }

    @Override
    public String process(Map<String, String> requestGetMap) {
        logger.info("start_command_filtr");
        StringBuilder stringBuffer = new StringBuilder();
        Map<Long, TravelVoucher> travelVouchers=travelVoucherService.filtr();
        stringBuffer.append("<h3>Filtr is default by cost >700</h3>");
        for(Map.Entry<Long, TravelVoucher> travelVoucher:travelVouchers.entrySet()){
            stringBuffer.append("<form method=\"POST\">"+travelVoucher.getValue().getName()+" "+travelVoucher.getValue().getCost()+" "+travelVoucher.getValue().getPeople()+"<input type='hidden' name='id' value='" +travelVoucher.getKey()+
                    "'/>"+"<input type='hidden' name='" + ApplicationConstants.COMMAND_PARAM+
                    "' value='delete'"+travelVoucher.getKey()+"'/>"+"<input type='submit' value='DELETE'/>"+"</form>");
        }

        return stringBuffer.toString();
    }
}

