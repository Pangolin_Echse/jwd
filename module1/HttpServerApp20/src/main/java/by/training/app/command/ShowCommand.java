package by.training.app.command;


import by.training.app.ApplicationConstants;
import by.training.app.entity.TravelVoucher;
import by.training.app.service.TravelVoucherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Map;

public class ShowCommand implements AppCommand {
    TravelVoucherService travelVoucherService;
    Logger logger= LogManager.getLogger();
    String name = "SHOW";

    public TravelVoucherService getTravelVoucherService() {
        return travelVoucherService;
    }
    public String getName() {
        return name;
    }

    public ShowCommand(TravelVoucherService travelVoucherService) {
        this.travelVoucherService = travelVoucherService;
    }

    @Override
    public String process(Map<String, String> requestGetMap) {
        logger.info("start_command_show");
        StringBuilder stringBuffer = new StringBuilder();
        Map<Long, TravelVoucher> travelVouchers=travelVoucherService.findAllTravelVouchers();
        for(Map.Entry<Long, TravelVoucher> travelVoucher:travelVouchers.entrySet()){
            stringBuffer.append("<form method=\"POST\">"+travelVoucher.getValue().getName()+" "+travelVoucher.getValue().getCost()+" "+travelVoucher.getValue().getPeople()+"<input type='hidden' name='id' value='" +travelVoucher.getKey()+
                    "'/>"+"<input type='hidden' name='" + ApplicationConstants.COMMAND_PARAM+
                    "' value='delete'"+travelVoucher.getKey()+"'/>"+"<input type='submit' value='DELETE'/>"+"</form>");
        }
        stringBuffer.append("<form method=\"POST\">" +
                "<input type=\"text\" name=\"name\" value=\"\" placeholder=\"Name\" required  minlength=\"1\">" +
                "<input type=\"number\" name=\"id\" value=\"\" placeholder=\"ID\" required >" +
                "<input type=\"number\" name=\"cost\" value=\"\" placeholder=\"COST\" required >" +
                "<input type=\"number\" name=\"people\" value=\"\" placeholder=\"PEOPLE\" required >" +
                "<input type=\"text\" name=\"type\" value=\"\" placeholder=\"TYPE\" required  minlength=\"1\">" +
                "<input type=\"hidden\" name=\"command\" value=\"add\">" +
                "<input type=\"submit\" value=\"ADD\"></form>\n");
        stringBuffer.append("<br><form method=\"GET\">" +
                "<input type=\"text\" name=\"fileName\" value=\"\" placeholder=\"fileName\" required  minlength=\"1\">" +
                "<input type=\"hidden\" name=\"command\" value=\"getFileName\">" +
                "<input type=\"submit\" value=\"GetFileName\"></form>\n");
        stringBuffer.append("<br><form method=\"GET\">" +
                "<h5>Want to sort me?<br>Here we go!) </h5>"+
                "<input type=\"text\" name=\"characters\" value=\"\" placeholder=\"byWhatYouWant\" required  minlength=\"1\">"+
                "<input type=\"hidden\" name=\"command\" value=\"sort\">" +
                "<input type=\"submit\" value=\"Sort!\"></form>\n");
        stringBuffer.append("<br><form method=\"POST\">" +
                "<h5>Want to filter?<br>Here we go!) </h5>"+
                "<input type=\"hidden\" name=\"command\" value=\"filtr\">" +
                "<input type=\"submit\" value=\"Filtr!\"></form>\n");
        return stringBuffer.toString();
    }
}
