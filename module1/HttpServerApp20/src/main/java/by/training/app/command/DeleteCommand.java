package by.training.app.command;
import by.training.app.service.TravelVoucherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class DeleteCommand implements AppCommand {
    private  String name = "DELETE";
    TravelVoucherService travelVoucherService;
     Logger logger= LogManager.getLogger();

    public String getName() {
        return name;
    }
    public TravelVoucherService getTravelVoucherService() {
        return travelVoucherService;
    }
    public DeleteCommand(TravelVoucherService travelVoucherService) {
        this.travelVoucherService = travelVoucherService;
    }

    @Override
    public String process(Map<String,String> requestGetMap) {
        logger.info("delete_command_start");
        String id = requestGetMap.get("id");
        if(id!=null) {
            boolean result = this.travelVoucherService.deleteVoucher(Long.parseLong(id));
            if(result){
                    logger.info("delete_command_success");
                return "IS_DELETED_NOW";
            }
        }
        logger.info("delete_command_failed");
        return "NO_SUCH_DATA";
    }
}

