package by.training.app.command;



import by.training.app.service.TravelVoucherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class DefaultCommand implements AppCommand {
    private String name = "DEFAULT";
    Logger logger= LogManager.getLogger();
    public String getName() {

        return name;
    }
    TravelVoucherService travelVoucherService;
    public TravelVoucherService getTravelVoucherService() {
        return travelVoucherService;
    }


    @Override
    public String process(Map<String, String> requestGetMap) {
         logger.info("start_command_default");

       return  "<br><form method=\"POST\">" +
                "<input type=\"hidden\" name=\"command\" value=\"showc\">" +
                "<input type=\"submit\" value=\"Show\"></form>\n";
    }
}
