package by.training.app.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.MessageFormat;
import java.util.Map;
import java.util.Optional;


public class SimpleCommandMaker implements CommandMaker {
    Map<String, AppCommand> commandStringMap;
    Logger logger= LogManager.getLogger();
    public SimpleCommandMaker(Map<String,AppCommand> commandStringMap) {
        this.commandStringMap = commandStringMap;
    }

    @Override
    /////////////// try to do pattern command
    public AppCommand makeCommand(String commandName) {
        logger.info(MessageFormat.format("command {0} has been build",commandName));
        return commandStringMap.get(commandName.toLowerCase());
    }
    @Override
    //////////////try to do  pattern facade
    public AppCommand findCommandName( Map<String, String> getParams){
             AppCommand command;

            String commandName =  Optional.ofNullable(getParams.get("command")).orElse("DEFAULT");

            command = makeCommand(commandName);

        return command;
    }

}