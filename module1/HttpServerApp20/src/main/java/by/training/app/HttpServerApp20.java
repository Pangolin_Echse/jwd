package by.training.app;

import by.training.app.command.CommandMaker;
import by.training.app.command.SimpleCommandMaker;
import by.training.app.controller.SimpleHandler;
import by.training.app.dao.Data;
import by.training.app.service.*;

import com.sun.net.httpserver.HttpServer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

import java.net.InetSocketAddress;


public class HttpServerApp20 {
    public static void main(String[] args) throws IOException {
        Logger logger= LogManager.getLogger(HttpServerApp20.class);
        final InetSocketAddress inetSocketAddress = new InetSocketAddress(ApplicationConstants.URL, ApplicationConstants.PORT);
        HttpServer server = HttpServer.create(inetSocketAddress, 0);
        Data data=new Data();
        TravelVoucherService travelVoucherService = new SimpleTravelVoucherService(data);
        FileService fileService= new FileService();
        ValidatorPost validatorPost=new SimpleValidatorPost(data);
        CommandMapFactory commands=new CommandMapFactory();
        ComparatorMapFactory comparatorMapFactory=new ComparatorMapFactory();
        CommandMaker commandMaker = new SimpleCommandMaker(commands.buildMap(travelVoucherService,fileService,validatorPost,comparatorMapFactory));
        server.createContext("/chill", new SimpleHandler(commandMaker));
        server.start();
        logger.info("Server started on http://" + ApplicationConstants.URL + ":" + ApplicationConstants.PORT + "/chill");
    }
}

