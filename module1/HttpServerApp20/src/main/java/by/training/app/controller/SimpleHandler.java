package by.training.app.controller;

import by.training.app.command.AppCommand;
import by.training.app.command.CommandMaker;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

public class SimpleHandler implements HttpHandler {
    private CommandMaker commandMaker;
    Logger logger = LogManager.getLogger();

    public SimpleHandler(CommandMaker commandMaker) {
        this.commandMaker = commandMaker;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        logger.info(httpExchange.getRequestURI());
        Map<String, String> getParams = parsRequest(httpExchange);
        String view;
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/defaultTemplate.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        String  templateView = bufferedReader.lines().collect(Collectors.joining());



        AppCommand command = commandMaker.findCommandName(getParams);
        String name = command.getName();
        if (!command.getName().equals("DEFAULT")) {
            String returnData = command.process(getParams);
            view = MessageFormat.format(templateView, name, returnData, commandMaker.makeCommand("SHOW").process(getParams));
        } else {
            String returnData = "WELCOME TO APP!!! :)";
            view = MessageFormat.format(templateView, name, returnData, commandMaker.makeCommand("DEFAULT").process(getParams));
        }
        httpExchange.sendResponseHeaders(200, view.length());
        OutputStream responseBody = httpExchange.getResponseBody();
        responseBody.write(view.getBytes());
        responseBody.flush();
        responseBody.close();
    }


    private Map<String, String> parsRequest(HttpExchange httpExchange) {
        StringBuilder buffer = new StringBuilder();
        if ("POST".equals(httpExchange.getRequestMethod())) {
            InputStream inputStream = httpExchange.getRequestBody();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            try (Scanner bufferScan = new Scanner(bufferedReader)) {
                while (bufferScan.hasNextLine()) {
                    buffer.append(bufferScan.nextLine());
                }
            }
        } else {
            String s = Optional.ofNullable(httpExchange.getRequestURI().getQuery()).orElse("");
            buffer.append(s);
        }

        return parsing(buffer);
    }

    private Map<String, String> parsing(StringBuilder buffer) {
        Map<String, String> stringMap = new HashMap<>();
        StringTokenizer st = new StringTokenizer(buffer.toString(), "&,=");
        int n = st.countTokens() / 2;
        for (int i = 0; i < n; i++) {
            stringMap.put(st.nextToken(), st.nextToken());
        }
        return stringMap;
    }
}
