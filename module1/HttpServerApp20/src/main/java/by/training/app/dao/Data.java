package by.training.app.dao;

import by.training.app.entity.TravelVoucher;
import by.training.app.entity.TravelVoucherType;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Data {

    static Map<Long, TravelVoucher> travelVouchers = new HashMap<>();

  public Data() {
        for (int i = 1; i < 10; i++) {
            if (i % 2 == 0) {
                travelVouchers.put((long) i, new TravelVoucher("voucher" + i,i+500,2, TravelVoucherType.CHILL));
            }
            if (i == 1) {
                travelVouchers.put((long) i, new TravelVoucher("voucher" + i, i+900,1,TravelVoucherType.CRUISE));
            }
            if (i == 3) {
                travelVouchers.put((long) i, new TravelVoucher("voucher" + i,i+600,3, TravelVoucherType.EXCURSION));
            }
            if (i == 5) {
                travelVouchers.put((long) i, new TravelVoucher("voucher" + i,i+100,1, TravelVoucherType.TREATMENT));
            }
            if (i == 7 || i == 9) {
                travelVouchers.put((long) i, new TravelVoucher("voucher" + i, i+800,2,TravelVoucherType.SHOPPING));
            }
        }
    }


    public Map<Long, TravelVoucher> getData() {
        return travelVouchers;
    }
    public Map<Long, TravelVoucher> filtrData(){ return  travelVouchers.entrySet()
            .stream()
            .filter(c -> c.getValue().getCost() > 700 )
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }
    public static void setData(Map<Long, TravelVoucher> travelVouchers) {
        Data.travelVouchers =travelVouchers;
    }

}


