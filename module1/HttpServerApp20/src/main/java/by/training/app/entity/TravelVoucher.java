package by.training.app.entity;

public class TravelVoucher {
    private int people;
    private int cost;
    private String name;
    private TravelVoucherType type;

    public TravelVoucher(String name,int cost,int people,TravelVoucherType type) {
        this.name = name;
        this.cost=cost;
        this.people=people;
        this.type = type;
    }


    public String getName() {
        return name;
    }
    public int getCost() {
        return cost;
    }
    public int getPeople() {
        return people;
    }
    public TravelVoucherType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "TravelVoucher{" +
                "people=" + people +
                "cost=" + cost +
                ", name='" + name + '\'' +
                ", type=" + type +
                '}';

    }

}


