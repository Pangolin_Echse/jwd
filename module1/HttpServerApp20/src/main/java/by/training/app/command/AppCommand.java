package by.training.app.command;

import by.training.app.service.TravelVoucherService;

import java.io.IOException;
import java.util.Map;

public interface AppCommand {

    String process(Map<String, String> params) throws IOException;
    String getName();
    TravelVoucherService getTravelVoucherService();
}
