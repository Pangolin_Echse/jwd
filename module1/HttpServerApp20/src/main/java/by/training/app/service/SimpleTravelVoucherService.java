package by.training.app.service;

import by.training.app.dao.Data;
import by.training.app.entity.TravelVoucher;
import by.training.app.entity.TravelVoucherType;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class SimpleTravelVoucherService implements TravelVoucherService {

    Data data;

    public SimpleTravelVoucherService(Data data) {
        this.data = data;
    }

    @Override
    public Map<Long, TravelVoucher> findAllTravelVouchers() {
        return data.getData();
    }

    @Override
    public boolean deleteVoucher(Long id) {
        if (data.getData().containsKey(id)) {
            data.getData().remove(id);
            return true;
        } else return false;
    }

    @Override
    public void addVoucher(Long id, String name, int cost, int people, TravelVoucherType type) {
        data.getData().put(id, new TravelVoucher(name, cost, people, type));
    }

    public Map<Long, TravelVoucher> filtr() {
        return data.filtrData();
    }
    public void sortVouchers(String byType , Comparator<? super Map.Entry<Long, TravelVoucher>> c) {

           Data.setData(data.getData().entrySet()
                    .stream()
                    .sorted(c)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new))
            );

        }
    }












