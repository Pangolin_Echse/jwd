package by.training.app.command;

;

import java.util.Map;

public interface CommandMaker {
    public AppCommand makeCommand(String commandName);
    public AppCommand findCommandName( Map<String, String> getParams);
}