package by.training.app.service;

public interface ValidatorPost {
    String validate(String id, String name, String type );
}
