package by.training.app.command;



import by.training.app.service.TravelVoucherService;
import by.training.app.service.ValidatorPost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Map;

import static by.training.app.entity.TravelVoucherType.*;

public class AddCommand implements AppCommand {
    private String name = "ADD";

    public String getName() {
        return name;
    }

    TravelVoucherService travelVoucherService;
    ValidatorPost simpleValidatorPost;
    public TravelVoucherService getTravelVoucherService() {
        return travelVoucherService;
    }

    Logger logger= LogManager.getLogger();
    public AddCommand(TravelVoucherService travelVoucherService,ValidatorPost simpleValidatorPost) {
        this.travelVoucherService = travelVoucherService;
        this.simpleValidatorPost=simpleValidatorPost;
    }
    @Override
    public String process(Map<String,String> requestGetMap) {
        logger.info("add_command_start");
        String id = requestGetMap.get("id");
        String valueName = requestGetMap.get("name");
        String type = requestGetMap.get("type");
        String c=requestGetMap.get("cost");
        String p=requestGetMap.get("people");
        int cost=Integer.parseInt(c);
        int people=Integer.parseInt(p);
        if(simpleValidatorPost.validate(id,valueName,type).equals("true")){
            Long i=Long.valueOf(id);//проверка на число производиться в input html
            this.travelVoucherService.addVoucher(i,valueName, cost,people, valueOf(type.toUpperCase()));
            logger.info("add_command_success");
            return "IS_ADDED_NOW";
        }
        logger.info("add_command_failed");
        return simpleValidatorPost.validate(id,valueName,type);
    }
}




