package by.training.app.command;

import by.training.app.service.FileService;
import by.training.app.service.TravelVoucherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoadFileCommand implements AppCommand {
    private String name = "GETFILE";
    FilePathValidator validator = new FilePathValidator();
    FileTypeValidator tValidator = new FileTypeValidator();
    FileExistValidator eValidator = new FileExistValidator();
    FileService fileService;
    TravelVoucherService travelVoucherService;
    Logger logger = LogManager.getLogger();

    public String getName() {

        return name;
    }
    public TravelVoucherService getTravelVoucherService() {
        return travelVoucherService;
    }
    public LoadFileCommand(TravelVoucherService travelVoucherService, FileService fileService) {
        this.travelVoucherService = travelVoucherService;
        this.fileService = fileService;
    }

    @Override
    public String process(Map<String, String> requestGetMap) throws IOException {
        logger.info("get_command_start");
        String fileName = URLDecoder.decode(requestGetMap.get("fileName"), StandardCharsets.UTF_8.toString());
        List<Integer> wrongLines;
        String s;
        try {

            validator.validate(fileName);
            tValidator.validate(fileName);
            eValidator.validate(fileName);
            wrongLines = fileService.readFile(Paths.get(fileName), travelVoucherService);
            Predicate<Integer> hasSize = t -> t > 0;
            if (hasSize.test(wrongLines.size())) {
                StringBuilder str = new StringBuilder();
                for (int i = 0; i < wrongLines.size(); i++) {
                    str.append("You have wrong input in line number  ").append(wrongLines.get(i));
                    str.append("\n");
                }
                str.append("\n Sorry,we don't add them");
                s = str.toString();
            } else {
                s = "EXIST";
            }
        } catch (ValidatorChainExeption u) {
            s = u.message;
        }
        return s;
    }


    class ValidatorChainExeption extends Exception {
        String message;

        ValidatorChainExeption(String message) {
            this.message = message;
        }
    }

    class FilePathValidator {
        public void validate(String fileName) throws ValidatorChainExeption {
            if (!fileName.matches("(([A-z]\\:(\\\\[^\\d+,\\.\\/\"':;*-+()&?](\\w|\\s)+)+)\\.([^\\d+,\\.\\/\"':;*-+()&?]\\w+))|(((\\/(\\w|\\s)+[^<>|.*:\"\\/])+\\.[^\\d+,\\.\\/\"':;*-+()&?]+))")) {
                throw new ValidatorChainExeption("Invalid input");
            }
        }
    }

    class FileExistValidator {
        public void validate(String fileName) throws ValidatorChainExeption {
            Predicate<String> exist = t -> Files.exists(Paths.get(t));
            if (exist.negate().test(fileName)) {
                throw new ValidatorChainExeption("NOT EXIST");
            }
        }
    }

    class FileTypeValidator {
        public void validate(String name) throws ValidatorChainExeption {
            Pattern p = Pattern.compile("\\.\\w+");
            Matcher m = p.matcher(name);
            m.find();
            String s = name.substring(m.start() + 1, m.end());
            if (!("txt".equals(s) || "cvs".equals(s))) {
                throw new ValidatorChainExeption("System not work with this");
            }
        }
    }
}







