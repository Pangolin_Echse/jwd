package by.training.app;

import by.training.app.ApplicationConstants;
import by.training.app.command.AppCommand;
import by.training.app.command.CommandMaker;
import by.training.app.command.SimpleCommandMaker;
import by.training.app.controller.SimpleHandler;
import by.training.app.dao.Data;
import by.training.app.service.*;

import com.sun.net.httpserver.HttpServer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;


public class HttpServerAppTest {
    Data data=new Data();
    TravelVoucherService travelVoucherService = new SimpleTravelVoucherService(data);
    FileService fileService= new FileService();
    ValidatorPost validatorPost=new SimpleValidatorPost(data);
    CommandMapFactory commands=new CommandMapFactory();
    ComparatorMapFactory comparatorMapFactory=new ComparatorMapFactory();
    CommandMaker commandMaker = new SimpleCommandMaker(commands.buildMap(travelVoucherService,fileService,validatorPost,comparatorMapFactory));
    @Before
    public  void Before() throws IOException {

     //  InetSocketAddress inetSocketAddress = new InetSocketAddress(ApplicationConstants.URL, ApplicationConstants.PORT);
     ///HttpServer server = HttpServer.create(inetSocketAddress, 0);


      // server.createContext("/chill", new SimpleHandler(commandMaker));
      //  server.start();
    }

    @Test
    public void addCommandTest() throws IOException {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("id", "45");
        requestMap.put("name", "someVaucher");
        requestMap.put("cost", "7975");
        requestMap.put("people", "9");
        requestMap.put("type", "chill");
        AppCommand command = commandMaker.makeCommand("ADD");
        int oldSize = command.getTravelVoucherService().findAllTravelVouchers().size();
        command.process(requestMap);
        int newSize = command.getTravelVoucherService().findAllTravelVouchers().size();
        assertEquals(oldSize + 1, newSize);
    }
    @Test
    public void addCommandWithIncorrectTypeTest() throws IOException {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("id", "45");
        requestMap.put("name", "someVaucher");
        requestMap.put("cost", "7975");
        requestMap.put("people", "9");
        requestMap.put("type", "cl.ljvlb/jb");
        AppCommand command = commandMaker.makeCommand("ADD");
        String s= command.process(requestMap);
        assertEquals(s,"THERE IS NOT SUCH TYPE");
    }
    @Test
    public void addCommandWithIncorrectIdTest() throws IOException {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("id", "2");
        requestMap.put("name", "someVaucher");
        requestMap.put("cost", "7975");
        requestMap.put("people", "9");
        requestMap.put("type", "cl.ljvlb/jb");
        AppCommand command = commandMaker.makeCommand("ADD");
        String s= command.process(requestMap);
        assertEquals(s,"YOU ALREADY HAVE ITEM WITH SAME KEY");
    }
    @Test
    public void deleteCommandTest() throws IOException {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("id", "4");
        AppCommand command = commandMaker.makeCommand("DELETE");
        int oldSize = command.getTravelVoucherService().findAllTravelVouchers().size();
        command.process(requestMap);
        int newSize = command.getTravelVoucherService().findAllTravelVouchers().size();
        assertEquals(oldSize , newSize+1);
    }
    @Test
    public void deleteNotExistCommandTest() throws IOException {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("id", "999");
        AppCommand command = commandMaker.makeCommand("DELETE");
        String s= command.process(requestMap);
        assertEquals(s, "NO_SUCH_DATA");
    }
    @Test
    public void LoadNotExistFileCommandTest() throws IOException {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("fileName", "/Users/kristina/Downloads/inklggliput1.txt");
        AppCommand command = commandMaker.makeCommand("getfilename");
        String answer = command.process(requestMap);
        assertEquals(answer , "NOT EXIST");
    }
    @Test
    public void LoadIncorrectInputFileCommandTest() throws IOException {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("fileName", "/Usufliyflyc");
        AppCommand command = commandMaker.makeCommand("getfilename");
        String answer = command.process(requestMap);
        assertEquals(answer , "Invalid input");
    }
    @Test
    public void LoadIncorrectTypeFileCommandTest() throws IOException {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("fileName", "/Users/kristina/Downloads/inklggliput1.jhfydyud");
        AppCommand command = commandMaker.makeCommand("getfilename");
        String answer = command.process(requestMap);
        assertEquals(answer , "System not work with this");
    }
}

